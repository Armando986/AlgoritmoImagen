<?php
   @require 'Zebra_Image.php';

class Imagenes {

      function imagenes_thumb($archivo, $temp_arch, $calidad=50){

      $image = new Zebra_Image();
      $prefijo = substr((rand()),0,6);

      if(isset($archivo)){

                        //imagenes
                        $origen =  "examples/images/".$prefijo."_".$archivo;
                        $image->source_path = $origen;
                        $image->target_path = "examples/results/t_".$prefijo."_".$archivo;
                        $image->jpeg_quality = $calidad;
                        $image->preserve_aspect_ratio = true;
                        $image->enlarge_smaller_images = true;
                        $image->preserve_time = true;

                        if (copy($temp_arch,$origen)) {

                                    $tamano=getimagesize($origen);
                                    $ancho=$tamano[0];
                                    $alto=$tamano[1];

                                    if($ancho  <  $alto ) {

                                          if (!$image->resize(320, 150, ZEBRA_IMAGE_CROP_TOPCENTER)) {

                                              // if there was an error, let's see what the error is about
                                                switch ($image->error) {
                                                case 1:
                                                    echo 'Source file could not be found!';
                                                    break;
                                                case 2:
                                                    echo 'Source file is not readable!';
                                                    break;
                                                case 3:
                                                    echo 'Could not write target file!';
                                                    break;
                                                case 4:
                                                    echo 'Unsupported source file format!';
                                                    break;
                                                case 5:
                                                    echo 'Unsupported target file format!';
                                                    break;
                                                case 6:
                                                    echo 'GD library version does not support target file format!';
                                                    break;
                                                case 7:
                                                    echo 'GD library is not installed!';
                                                    break;
                                               }
                                             }
                                    else {

                                        echo 'Success!';

                                    }

                                    }else{
                                          if (!$image->resize(320, 150, ZEBRA_IMAGE_CROP_CENTER)) {
                                            switch ($image->error) {
                                            case 1:
                                                echo 'Source file could not be found!';
                                                break;
                                            case 2:
                                                echo 'Source file is not readable!';
                                                break;
                                            case 3:
                                                echo 'Could not write target file!';
                                                break;
                                            case 4:
                                                echo 'Unsupported source file format!';
                                                break;
                                            case 5:
                                                echo 'Unsupported target file format!';
                                                break;
                                            case 6:
                                                echo 'GD library version does not support target file format!';
                                                break;
                                            case 7:
                                                echo 'GD library is not installed!';
                                                break;

                                        }
                                    }
                                    else {

                                        echo 'Success!';
                                        unlink($origen); //borrar
                                      }
                                    }




                        }

                }
                else{
                   return 0;
                }

    }

    function imagenes_grande($archivo, $temp_arch, $calidad=50){
       $image = new Zebra_Image();
       $prefijo = substr((rand()),0,6);

       if(isset($archivo)){
                        $origen =  "examples/images/".$prefijo."_".$archivo;
                        $image->source_path = $origen;
                        $image->target_path = "examples/results/G_".$prefijo."_".$archivo;
                        $image->jpeg_quality = $calidad;
                        $image->preserve_aspect_ratio = true;
                        $image->enlarge_smaller_images = true;
                        $image->preserve_time = true;

                        if (copy($temp_arch,$origen)) {



                                          if (!$image->resize(600)) {

                                              // if there was an error, let's see what the error is about
                                                switch ($image->error) {
                                                case 1:
                                                    echo 'Source file could not be found!';
                                                    break;
                                                case 2:
                                                    echo 'Source file is not readable!';
                                                    break;
                                                case 3:
                                                    echo 'Could not write target file!';
                                                    break;
                                                case 4:
                                                    echo 'Unsupported source file format!';
                                                    break;
                                                case 5:
                                                    echo 'Unsupported target file format!';
                                                    break;
                                                case 6:
                                                    echo 'GD library version does not support target file format!';
                                                    break;
                                                case 7:
                                                    echo 'GD library is not installed!';
                                                    break;
                                               }
                                             }
                                    else {
                                        echo 'Success!';
                                        unlink($origen); //borrar
                                    }
                              }
                        }
                else{
                   return 0;
                }

    }
}

?>
